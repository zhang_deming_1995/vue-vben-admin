import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  {
    title: '类型',
    dataIndex: 'type',
    width: 150,
    sorter: true,
    customRender: ({ record }) => {
      const status = record.type;
      switch (status) {
        case 1:
          return h(Tag, { color: 'green' }, () => '省内城市');
        case 2:
          return h(Tag, { color: 'green' }, () => '副省级城市');
        case 3:
          return h(Tag, { color: 'green' }, () => '人口过千万城市');
        case 4:
          return h(Tag, { color: 'green' }, () => '超大城市');
        case 5:
          return h(Tag, { color: 'green' }, () => '特大城市');
        case 6:
          return h(Tag, { color: 'green' }, () => '山东省');
        case 7:
          return h(Tag, { color: 'green' }, () => '全国');
        case 8:
          return h(Tag, { color: 'green' }, () => '主要对比城市');
        default:
          break;
      }
    },
  },
  {
    title: '年份',
    dataIndex: 'year',
    sorter: true,
  },
  {
    title: '城市',
    dataIndex: 'cityName',
    sorter: true,
  },
  {
    title: '人口规模',
    dataIndex: 'populationNumber',
    sorter: true,
  },
  {
    title: '经度',
    dataIndex: 'longitude',
    sorter: true,
  },
  {
    title: '纬度',
    dataIndex: 'latitude',
    sorter: true,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'type',
    label: '类型',
    component: 'Select',
    componentProps: {
      options: [
        { label: '省内城市', value: 1 },
        { label: '副省级城市', value: 2 },
        { label: '人口过千万城市', value: 3 },
        { label: '超大城市', value: 4 },
        { label: '特大城市', value: 5 },
        { label: '山东省', value: 6 },
        { label: '全国', value: 7 },
        { label: '主要对比城市', value: 8 },
      ],
    },
    colProps: { span: 3 },
  },
  {
    field: 'year',
    label: '年份',
    component: 'InputNumber',
    componentProps: {
      decimalSeparator: false,
    },
    colProps: { span: 3 },
  },
  {
    field: 'cityName',
    label: '城市',
    component: 'Input',
    colProps: { span: 3 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'type',
    required: true,
    label: '类型',
    component: 'Select',
    componentProps: {
      options: [
        { label: '省内城市', value: 1 },
        { label: '副省级城市', value: 2 },
        { label: '人口过千万城市', value: 3 },
        { label: '超大城市', value: 4 },
        { label: '特大城市', value: 5 },
        { label: '山东省', value: 6 },
        { label: '全国', value: 7 },
        { label: '主要对比城市', value: 8 },
      ],
    },
  },
  {
    field: 'year',
    label: '年份',
    component: 'InputNumber',
    componentProps: {
      decimalSeparator: false,
    },
    required: true,
  },
  {
    field: 'cityName',
    label: '城市',
    component: 'Input',
    required: true,
  },
  {
    field: 'populationNumber',
    label: '人口规模',
    component: 'InputNumber',
    required: true,
  },
  {
    field: 'longitude',
    label: '经度',
    component: 'InputNumber',
    required: true,
  },
  {
    field: 'latitude',
    label: '纬度',
    component: 'InputNumber',
    required: true,
  },
];
