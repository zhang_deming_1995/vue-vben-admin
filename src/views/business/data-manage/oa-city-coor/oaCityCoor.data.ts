import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';

export const columns: BasicColumn[] = [
  {
    title: '城市',
    dataIndex: 'city',
    align: 'left',
    sorter: true,
  },
  {
    title: '经度',
    dataIndex: 'longitude',
    sorter: true,
  },
  {
    title: '纬度',
    dataIndex: 'latitude',
    sorter: true,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'city',
    label: '城市',
    component: 'Input',
    colProps: { span: 4 },
  },
  {
    field: 'longitude',
    label: '经度',
    component: 'InputNumber',
    colProps: { span: 4 },
  },
  {
    field: 'latitude',
    label: '纬度',
    component: 'InputNumber',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'city',
    label: '城市',
    component: 'Input',
    required: true,
  },
  {
    field: 'longitude',
    label: '经度',
    component: 'InputNumber',
    required: true,
  },
  {
    field: 'latitude',
    label: '纬度',
    component: 'InputNumber',
    required: true,
  },
];
