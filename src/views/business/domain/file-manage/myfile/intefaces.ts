// 文件对象
export interface IFileObj {
  filename: File;
  progress: number;
  status: string;
  uploadId: number;
  md5: number;
}
