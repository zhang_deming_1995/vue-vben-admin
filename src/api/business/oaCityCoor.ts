import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  //查询单条
  SEARCH = '/base-service/data/oa_city_coor/search',
  //列表
  PAGE = '/base-service/data/oa_city_coor/page',
  //新增
  CREATE = '/base-service/data/oa_city_coor/create',
  //修改
  UPDATE = '/base-service/data/oa_city_coor/update',
  //删除
  DELETE = '/base-service/data/oa_city_coor/delete',
  //导入
  IMPORT = '/base-service/data/oa_city_coor/import',
  //导出模板
  TEMPLET = '/base-service/data/oa_city_coor/templet',
  //导出
  EXPORT = '/base-service/data/oa_city_coor/export',
}

//获取部门信息的信息
export const searchApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.SEARCH + '/' + params,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

/**
 * 账户列表
 * @param params
 * @param mode
 * @returns
 */
export const pageApi = (params, mode: ErrorMessageMode) => {
  return defHttp.request(
    {
      url: Api.PAGE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//新增账户
export const createApi = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.CREATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//修改账户
export const updateApi = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.UPDATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//删除账户
export const deleteApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.DELETE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//导出模板
export const templetApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.TEMPLET,
      backService: true,
      method: 'GET',
      responseType: 'blob',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//导出
export const exportApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.EXPORT,
      backService: true,
      method: 'GET',
      responseType: 'blob',
      params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//导入
export const importApi = (formdata) => {
  return defHttp.uploadFile(
    {
      url: Api.IMPORT,
      backService: true,
      method: 'POST',
      data: formdata,
      headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
    },
    {
      file: formdata,
    },
  );
};
