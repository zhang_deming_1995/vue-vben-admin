import { ErrorMessageMode } from '/#/axios';
import { defHttp } from '/@/utils/http/axios';

enum Api {
  //初始化传输任务
  CREATE_TASK = '/base-service/chunck_upload/createTask',
  //修改传输任务信息
  UPDATE_TASK = '/base-service/chunck_upload/updateTask',
  //分页查询上传的资源
  LIST = '/base-service/chunck_upload/list',
  //上传
  UPLOAD = '/base-service/chunck_upload/upload',
  //分片初始化
  UPLOAD_INIT = '/base-service/chunck_upload/init',
  //完成上传 分片文件汇总
  UPLOAD_MERGE = '/base-service/chunck_upload/merge',
  //下载
  DOWNLOAD = '/base-service/chunck_upload/getUploadUrl/',
  //预览
  PREVIEW = '/base-service/chunck_upload/openOfficePreview/',
  //修改文件信息
  UPDATE = '/base-service/chunck_upload/update',
  //删除文件
  DELETE = '/base-service/chunck_upload/delete',
  //生成分享链接
  IMPORT = '/base-service/chunck_upload/generate_shared_links',
  //文件及层级
  FOLDER_TREE = '/base-service/chunck_upload/folder/tree',
  //创建文件夹
  FOLDER_CREATE = '/base-service/chunck_upload/folder/create',
  //修改文件夹
  FOLDER_UPDATE = '/base-service/chunck_upload/folder/update',
  //删除文件夹
  FOLDER_DELETE = '/base-service/chunck_upload/folder/delete',
  //删除文件夹
  SIMPLE_FILE_MINIO_UPLOAD_API = '/base-service/simple_upload/minio/api',
}

//获取文件和文件夹
export const listApi = (params) => {
  return defHttp.request({
    url: Api.LIST,
    backService: true,
    method: 'POST',
    data: params,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
};
//文件夹层级
export const getFolderTreeApi = (params) => {
  return defHttp.request({
    url: Api.FOLDER_TREE,
    backService: true,
    method: 'POST',
    data: params,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
};
//创建文件夹
export const createFolderApi = (params) => {
  return defHttp.request({
    url: Api.FOLDER_CREATE,
    backService: true,
    method: 'POST',
    data: params,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
};
//修改文件夹
export const updateFolderApi = (params) => {
  return defHttp.request({
    url: Api.FOLDER_UPDATE,
    backService: true,
    method: 'POST',
    data: params,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
};
//删除文件夹
export const deleteFolderApi = (params) => {
  return defHttp.request({
    url: Api.FOLDER_DELETE,
    backService: true,
    method: 'POST',
    data: params,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
};
//修改文件信息
export const updateFileApi = (params) => {
  return defHttp.request({
    url: Api.UPDATE,
    backService: true,
    method: 'POST',
    data: params,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
};
//删除文件
export const deleteFileApi = (params) => {
  return defHttp.request({
    url: Api.DELETE,
    backService: true,
    method: 'POST',
    data: params,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
};
//创建传输任务
export const createTaskApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.CREATE_TASK,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//更新任务状态
export const updateTaskApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.UPDATE_TASK,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
//初始化上传
export const initUploadApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.UPLOAD_INIT,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
//初始化上传
export const minioUploadApi = (url, params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.minioUpload(
    {
      withToken: false,
      url: url,
      backService: true,
      method: 'PUT',
      data: params,
      headers: {
        'Content-Type': 'application/octet-stream',
      },
    },
    {
      withToken: false,
      errorMessageMode: mode,
    },
  );
};
//合并文件
export const mergeFileApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.UPLOAD_MERGE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
//获取下载地址
export const downloadApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.DOWNLOAD + params,
      backService: true,
      method: 'GET',
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
//获取预览地址
export const previewApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      timeout: 300000,
      url: Api.PREVIEW + params,
      backService: true,
      method: 'GET',
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
//获取minio上传地址
export const simpleFileMinioUploadApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      timeout: 300000,
      url: Api.SIMPLE_FILE_MINIO_UPLOAD_API,
      backService: true,
      params,
      method: 'GET',
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
