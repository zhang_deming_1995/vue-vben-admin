import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import { RoleListGetResultModel } from './model/roleModel';

enum Api {
  //列表
  PAGE_TABLE = '/base-service/page/search',
}

/**
 * 账户列表
 * @param params
 * @param mode
 * @returns
 */
export const demoListApi = (params, mode: ErrorMessageMode) => {
  return defHttp.request<RoleListGetResultModel>(
    {
      url: Api.PAGE_TABLE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
