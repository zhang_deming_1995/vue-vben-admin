import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  //用户列表
  USER_LIST = '/base-service/chat_online/user_list',
  // 查询自己和对应用户的聊天记录
  SEARCH_CACHE = '/base-service/chat_online/search_cache',
}
//获取全部用户列表
export const getUserListApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.USER_LIST,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
//获取与指定用户的聊天信息
export const getMessListApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.SEARCH_CACHE + '/' + params,
      backService: true,
      method: 'GET',
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
