import { defHttp } from '/@/utils/http/axios';
import { MenuListItem, MenuListGetResultModel } from './model/resourcesModel';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  //列表
  MENU_LIST = '/auth-service/resources/page',
  //新增
  MENU_CREATE = '/auth-service/resources/create',
  //修改
  MENU_UPDATE = '/auth-service/resources/update',
  //删除
  MENU_DELETE = '/auth-service/resources/delete',
  //启用\禁用前的校验
  MENU_CHECK_INVALID = '/auth-service/resources/checkInvalid',
  //启用\禁用
  MENU_INVALID = '/auth-service/resources/invalidReource',
  //查询单条
  MENU_SEARCH = '/auth-service/resources/search',
}

/**
 * 部门列表
 * @param params
 * @param mode
 * @returns
 */
export const getMenuList = (params?: MenuListItem, mode?: ErrorMessageMode) => {
  return defHttp.request<MenuListGetResultModel>(
    {
      url: Api.MENU_LIST,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//获取部门信息的信息
export const searchMenu = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request<MenuListGetResultModel>(
    {
      url: Api.MENU_SEARCH + '/' + params,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//新增部门
export const createMenu = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.MENU_CREATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//修改部门
export const updateMenu = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.MENU_UPDATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//删除部门
export const deleteMenu = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.MENU_DELETE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//启用禁用前的校验
export const checkInvalidMenu = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.MENU_CHECK_INVALID,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//启用禁用
export const invalidResource = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.MENU_INVALID,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
