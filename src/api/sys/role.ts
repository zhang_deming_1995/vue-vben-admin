import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';
import { RoleListGetResultModel } from './model/roleModel';

enum Api {
  //列表
  ROLE_LIST = '/auth-service/roles/page',
  //个人信息
  ROLE_SEARCH = '/auth-service/roles/search',
  //新增账户
  ROLE_CREATE = '/auth-service/roles/create',
  //修改账户
  ROLE_UPDATE = '/auth-service/roles/update',
  //删除账户
  ROLE_DELETE = '/auth-service/roles/delete',
}

/**
 * 账户列表
 * @param params
 * @param mode
 * @returns
 */
export const getRoleList = (params, mode: ErrorMessageMode) => {
  return defHttp.request<RoleListGetResultModel>(
    {
      url: Api.ROLE_LIST,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//获取用户的信息
export const searchRole = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ROLE_SEARCH + '/' + params,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//新增账户
export const createRole = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ROLE_CREATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//修改账户
export const updateRole = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ROLE_UPDATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//删除账户
export const deleteRole = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ROLE_DELETE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
