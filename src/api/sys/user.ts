import { defHttp } from '/@/utils/http/axios';
import { LoginParams, LoginResultModel, GetUserInfoModel } from './model/userModel';

import { ErrorMessageMode } from '/#/axios';

// enum Api {
//   Login = '/login',
//   Logout = '/logout',
//   GetUserInfo = '/getUserInfo',
//   GetPermCode = '/getPermCode',
// }

enum Api {
  Login = '/auth-service/oauth/token',
  Logout = '/auth-service/user/logout',
  GetUserInfo = '/auth-service/user/getUserInfo',
  GetPermCode = '/auth-service/user/getPermissions',
  CreateSmsCode = '/auth-service/user/unauth/sms/create',
  GetUserPublicMsg = '/auth-service/user/getUserPublicMsgById',
}

/**
 * @description: user login api
 */
export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.request<LoginResultModel>(
    {
      url: Api.Login,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: getUserInfo
 */
export function getUserInfo() {
  return defHttp.request<GetUserInfoModel>({
    url: Api.GetUserInfo,
    backService: true,
    method: 'GET',
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
  // return defHttp.get<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}

export function createSmsCode(data) {
  return defHttp.request({
    url: Api.CreateSmsCode,
    backService: true,
    method: 'POST',
    data: data,
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
  // return defHttp.get<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}

export function getPermCode() {
  return defHttp.request<string[]>({
    url: Api.GetPermCode,
    backService: true,
    method: 'GET',
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
  // return defHttp.get<string[]>({ url: Api.GetPermCode });
}

export function doLogout() {
  return defHttp.request({
    url: Api.Logout,
    backService: true,
    method: 'POST',
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
  });
  // return defHttp.get({ url: Api.Logout });
}

export function getUserPublicMsgById(params) {
  return defHttp.request(
    {
      url: Api.GetUserPublicMsg + '?id=' + params,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    });
};