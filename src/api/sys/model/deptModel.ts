import { BasicFetchResult } from '/@/api/model/baseModel';

export type DeptParams = {
  name?: string;
  status?: string;
};

export interface DeptListItem {
  id: string;
  sortNumber: string;
  createDate: string;
  remark: string;
  name: string;
  status: number;
}

export type DeptListGetResultModel = BasicFetchResult<DeptListItem>;
