import { BasicFetchResult } from '/@/api/model/baseModel';

export type MenuParams = {
  name?: string;
  status?: string;
};

export interface MenuListItem {
  id: string;
  sortNumber: string;
  createDate: string;
  remark: string;
  name: string;
  status: number;
}

export type MenuListGetResultModel = BasicFetchResult<MenuListItem>;
