import { BasicPageParams } from '/@/api/model/baseModel';

export interface RoleListItem {
  id: string;
  roleName: string;
  roleValue: string;
  status: number;
  orderNo: string;
  createTime: string;
}

export type RoleParams = {
  roleName?: string;
  status?: string;
};

export type RolePageParams = BasicPageParams & RoleParams;
export type RoleListGetResultModel = RoleListItem[];
