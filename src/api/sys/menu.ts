import { defHttp } from '/@/utils/http/axios';
import { getMenuListResultModel } from './model/menuModel';

enum Api {
  // GetMenuList = '/getMenuList',
  GetMenuList = '/auth-service/user/getMenuList',
}

/**
 * @description: Get user menu based on id
 */

export const getMenuList = () => {
  //获取菜单列表

  const menus = defHttp.request<getMenuListResultModel>({
    url: Api.GetMenuList,
    backService: true,
    method: 'GET',
  });
  return menus;
  // return defHttp.get<getMenuListResultModel>({ url: Api.GetMenuList });
};
