import { defHttp } from '/@/utils/http/axios';
import { DeptListItem, DeptListGetResultModel } from './model/deptModel';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  //部门列表
  DEPT_LIST = '/auth-service/dept/page',
  //新增部门
  DEPT_CREATE = '/auth-service/dept/create',
  //修改部门信息
  DEPT_UPDATE = '/auth-service/dept/update',
  //删除部门信息
  DEPT_DELETE = '/auth-service/dept/delete',
  //启用\禁用前的校验
  DEPT_CHECK_INVALID = '/auth-service/dept/checkInvalid',
  //启用\禁用
  DEPT_INVALID = '/auth-service/dept/invalidDept',
  //查询单条
  DEPT_SEARCH = '/auth-service/dept/search',
}

/**
 * 部门列表
 * @param params
 * @param mode
 * @returns
 */
export const getDeptList = (params?: DeptListItem, mode?: ErrorMessageMode) => {
  return defHttp.request<DeptListGetResultModel>(
    {
      url: Api.DEPT_LIST,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//获取部门信息的信息
export const searchDept = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request<DeptListGetResultModel>(
    {
      url: Api.DEPT_SEARCH + '/' + params,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//新增部门
export const createDept = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.DEPT_CREATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//修改部门
export const updateDept = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.DEPT_UPDATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//删除部门
export const deleteDept = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.DEPT_DELETE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//启用禁用前的校验
export const checkInvalidDept = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.DEPT_CHECK_INVALID,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//启用禁用
export const invalidDept = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.DEPT_INVALID,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
