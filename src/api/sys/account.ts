import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  //用户列表
  ACCOUNT_LIST = '/auth-service/user/page',
  //个人信息
  ACCOUNT_INFO = '/auth-service/user/getUserInfo',
  //个人信息
  ACCOUNT_SEARCH = '/auth-service/user/search',
  //新增账户
  ACCOUNT_CREATE = '/auth-service/user/create',

  UNAUTH_ACCOUNT_CREATE = '/auth-service/user/unauth/create',
  //修改账户
  ACCOUNT_UPDATE = '/auth-service/user/update',
  //删除账户
  ACCOUNT_DELETE = '/auth-service/user/delete',
  //个人修改基本信息
  ACCOUNT_UPDATE_USER_INFO = '/auth-service/user/updateUserInfo',
  //头像上传下载地址
  AVATAR_UPDOWNLOAD = '/auth-service/user/minio/avatar/api',
  SESSION_TIMEOUT = '/user/sessionTimeout',
  TOKEN_EXPIRED = '/user/tokenExpired',
}

/**
 * 账户列表
 * @param params
 * @param mode
 * @returns
 */
export const getAccountList = (params, mode: ErrorMessageMode) => {
  return defHttp.request(
    {
      url: Api.ACCOUNT_LIST,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//获取用户的信息
export const accountInfoApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ACCOUNT_INFO,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//获取用户的信息
export const searchAccount = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ACCOUNT_SEARCH + '/' + params,
      backService: true,
      method: 'GET',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//新增账户
export const createAccount = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ACCOUNT_CREATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//新增账户
export const unAuthCreateAccount = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.UNAUTH_ACCOUNT_CREATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//修改账户
export const updateAccount = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ACCOUNT_UPDATE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//修改个人基本信息
export const updateAccountUserInfo = (params?: JSON, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ACCOUNT_UPDATE_USER_INFO,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};
//删除账户
export const deleteAccount = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      url: Api.ACCOUNT_DELETE,
      backService: true,
      method: 'POST',
      data: params,
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

//获取minio头像上传地址
export const avatarFileUploadApi = (params, mode: ErrorMessageMode = 'modal') => {
  return defHttp.request(
    {
      timeout: 300000,
      url: Api.AVATAR_UPDOWNLOAD,
      backService: true,
      params,
      method: 'GET',
      headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    },
    {
      errorMessageMode: mode,
    },
  );
};

export const sessionTimeoutApi = () => defHttp.post<void>({ url: Api.SESSION_TIMEOUT });

export const tokenExpiredApi = () => defHttp.post<void>({ url: Api.TOKEN_EXPIRED });
